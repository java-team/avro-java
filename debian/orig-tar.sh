#!/bin/sh -e
#
# Removes unwanted content from the upstream sources.
# Called by uscan with '--upstream-version' <version> <file>
#

VERSION=$2
TAR=../avro-java_$VERSION.orig.tar.xz
DIR=avro-$VERSION
TAG=$(echo "release-$VERSION" | sed -re's/~(alpha|beta|rc)/-\1-/')

svn export http://svn.apache.org/repos/asf/avro/tags/${TAG} $DIR
XZ_OPT=--best tar -c -v -J -f $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude 'lang/c/*' \
    --exclude 'lang/c++/*' \
    --exclude 'lang/csharp/*' \
    --exclude 'lang/js/*' \
    --exclude 'lang/perl/*' \
    --exclude 'lang/php/*' \
    --exclude 'lang/py*' \
    --exclude 'lang/ruby/*' \
    --exclude '*/jquery-*.min.js' \
    --exclude '*/protovis-*.js' \
    $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
